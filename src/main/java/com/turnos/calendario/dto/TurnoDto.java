package com.turnos.calendario.dto;

import java.util.Date;

public class TurnoDto {
	private Date fecha;
	private Date hora;
	
	public TurnoDto() {
		// TODO Auto-generated constructor stub
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Date getHora() {
		return hora;
	}

	public void setHora(Date hora) {
		this.hora = hora;
	}

	@Override
	public String toString() {
		return "TurnoDto [fecha=" + fecha + ", hora=" + hora + "]";
	}

	
	

}
