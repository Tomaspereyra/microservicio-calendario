package com.turnos.calendario.controller;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.turnos.calendario.model.Turno;
import com.turnos.calendario.service.ITurnoService;
import com.turnos.calendario.util.Constante;
import com.turnos.calendario.util.RestResponse;

@RestController
//@CrossOrigin(origins = "http://localhost:4200") //  the annotation enables Cross-Origin Resource Sharing (CORS) on the server.
public class TurnoController {
	@Autowired
	private ITurnoService turnoService;
	
	@Autowired
	private RestResponse response;
	
	@PostMapping("/turnos")
	public RestResponse guardarTurno(@RequestBody Map<String, ? extends Object> body) {
		try {
		Turno turno = new Turno();
		turno.setNombreCliente(body.get("nombrePF").toString());
		turno.setApellidoCliente(body.get("apellidoPF").toString());
		turno.setEmailCliente(body.get("emailPF").toString());
		Map<String,String> fecha = new HashMap<>();
		fecha = (Map<String, String>) body.get("fecha");
		
		String año=String.valueOf(fecha.get("year"));
		String mes=String.valueOf(fecha.get("month"));
		String dia=String.valueOf(fecha.get("day"));
		turno.setFecha(new SimpleDateFormat("yyyy-MM-dd").parse(año+"-"+mes+"-"+dia));
		Map<String,String> hora = new HashMap<>();
		hora = (Map<String, String>) body.get("hora");
		turno.setHora(new SimpleDateFormat("HH:mm:ss").parse(hora.get("hora").toString()+":"+hora.get("minutos").toString()+":00"));
		turno.setUsuarioComercio(body.get("usuarioComercio").toString());
		response = this.turnoService.guardarTurno(turno);
		}catch(Exception e) {
			response.setResponseCode(Constante.RESPONSE_COD_500);
			response.setResponseMessage("Ocurrio un error en el controller guardar turno");
			
			
		}
		
		return response;
		
		
	}
	@GetMapping("/turnos")
	public RestResponse traerTurnos(@RequestParam String usuarioComercio, @RequestParam String fecha) {
		try {
			response = this.turnoService.traerTurnos(usuarioComercio,new SimpleDateFormat("yyyy-MM-dd").parse(fecha));
			
		}catch(Exception e) {
			response.setResponseCode(Constante.RESPONSE_COD_500);
			response.setResponseMessage("Ocurrio un error al traer los turnos. Turno controller");
			
		}
		
		return response;
	}
	@GetMapping("/buscar-turno-mes")
	public RestResponse traerTurnosPorMesAño(@RequestParam String usuarioComercio, @RequestParam Integer mes, @RequestParam Integer año) {
		try {
			response = this.turnoService.traerTurnos(usuarioComercio, mes, año);
			
		}catch(Exception e) {
			response.setResponseCode(Constante.RESPONSE_COD_500);
			response.setResponseMessage("Ocurrio un error al traer los turnos. Turno controller");

			
		}
		return response;
	}
	@DeleteMapping("/borrar/{id}")
	public RestResponse eliminarTurno(@PathVariable Integer id) {
		try {
			response = this.turnoService.borrarTurno(id);
			response.setData(null);
		}catch(Exception e)
		{
			response.setResponseCode(Constante.RESPONSE_COD_500);
			response.setResponseMessage("Ocurrio un error al borrar el turno con id:"+id+". Turno controller");
		}
		return response;
	}

}
