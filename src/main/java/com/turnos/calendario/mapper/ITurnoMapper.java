package com.turnos.calendario.mapper;

import java.util.List;
import java.util.Map;

import com.turnos.calendario.model.Turno;

public interface ITurnoMapper {
	public Map<String,Object> soloFechaYHora(List<Turno> turnos);
}
