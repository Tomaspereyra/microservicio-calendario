package com.turnos.calendario.mapper.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.turnos.calendario.dto.TurnoDto;
import com.turnos.calendario.mapper.ITurnoMapper;
import com.turnos.calendario.model.Turno;
@Component
public class TurnoMapperImpl implements ITurnoMapper{

	

	@Override
	public Map<String,Object> soloFechaYHora(List<Turno> turnos) {
		List<TurnoDto> turnosDto = new ArrayList<>();
		Map<String,Object> data = new HashMap<>();
		TurnoDto dto = new TurnoDto();
		for(Turno t:turnos) {
			dto.setFecha(t.getFecha());
			dto.setHora(t.getHora());
			turnosDto.add(dto);
		}
		data.put("turnos", turnosDto);
		return data;
	}
	
	

}
