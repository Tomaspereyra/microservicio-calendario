package com.turnos.calendario.service;

import java.util.Date;
import java.util.Map;

import com.turnos.calendario.model.Turno;
import com.turnos.calendario.util.RestResponse;

public interface ITurnoService {
	
	public RestResponse traerTurnos(String usuarioComercio, Date fecha);
	
	public RestResponse guardarTurno(Turno turno);
	
	public RestResponse traerTurnos(String usuarioComercio, Integer mes,Integer año);
	
	public RestResponse borrarTurno(Integer id);
	

}
