package com.turnos.calendario.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.turnos.calendario.dto.TurnoDto;
import com.turnos.calendario.mapper.ITurnoMapper;
import com.turnos.calendario.model.Turno;
import com.turnos.calendario.repository.ITurnoRepository;
import com.turnos.calendario.service.ITurnoService;
import com.turnos.calendario.util.Constante;
import com.turnos.calendario.util.RestResponse;

@Service
public class TurnoServiceImpl implements ITurnoService{
	
	@Autowired
	private ITurnoRepository turnoRepository;
	@Autowired
	private RestResponse response;
	
	@Autowired
	private ITurnoMapper turnoMapper;
	
	@Value("${eureka.instance.instance-id}")
	private String instancia;

	@Override
	@Transactional(readOnly = true)
	public RestResponse traerTurnos(String usuarioComercio, Date fecha) {
		try {
			List<Turno> turnos = this.turnoRepository.traerTurnosPorUsuarioYFecha(usuarioComercio, fecha);
			Map<String,List<Turno>> data = new HashMap<>();
			data.put("turnos", turnos);
			response.setResponseCode(Constante.RESPONSE_COD_200);
			response.setResponseMessage("Instancia"+this.instancia);
			response.setData(data);
			
		}catch(Exception e) {
			response.setResponseCode(Constante.RESPONSE_COD_500);
			response.setResponseMessage("Ocurrio en error al consultar en la base de datos");
			
		}
		return response;
	}

	@Override
	@Transactional()
	public RestResponse guardarTurno(Turno turno) {
		try {		
			
			this.turnoRepository.save(turno);
			response.setResponseCode(Constante.RESPONSE_COD_200);
			response.setResponseMessage("OK");
			
		}catch(Exception e) {
			response.setResponseCode(Constante.RESPONSE_COD_500);
			response.setResponseMessage("Ocurrio un error al intentar guardar el turno");
			
		}
		return response;
	}

	@Override
	@Transactional(readOnly = true)
	public RestResponse traerTurnos(String usuarioComercio, Integer mes, Integer año) {
		try {
			List<Turno> turnos =this.turnoRepository.traerTurnosPorUsuarioMesAño(usuarioComercio, mes,año);
			response.setData(turnoMapper.soloFechaYHora(turnos));
			response.setResponseCode(200);
			response.setResponseMessage("OK");
		}catch(Exception e) {
			response.setResponseCode(Constante.RESPONSE_COD_500);
			response.setResponseMessage("Ocurrio un error al intentar guardar el turno");	
			}
		return response;
	}

	@Override
	@Transactional()
	public RestResponse borrarTurno(Integer id) {
		try {
			
			this.turnoRepository.deleteById(id);
			response.setResponseCode(200);
			response.setResponseMessage("OK");
		}catch(Exception e) {
			response.setResponseCode(Constante.RESPONSE_COD_500);
			response.setResponseMessage("Ocurrio un error al intentar eliminar el turno");	
			}
		return response;
	}

}
