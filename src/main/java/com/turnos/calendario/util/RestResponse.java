package com.turnos.calendario.util;

import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class RestResponse {
	
	private Integer responseCode;
	private String responseMessage;
	private Map<String, ? extends Object> data;
	public Integer getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(Integer responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public Map<String, ? extends Object> getData() {
		return data;
	}
	public void setData(Map<String, ? extends Object> data) {
		this.data = data;
	}
	@Override
	public String toString() {
		return "RestResponse [responseCode=" + responseCode + ", responseMessage=" + responseMessage + ", data=" + data
				+ "]";
	}


}
