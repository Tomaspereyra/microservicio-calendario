package com.turnos.calendario.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.turnos.calendario.model.Turno;

public interface ITurnoRepository extends JpaRepository<Turno,Integer>{
	@Query("SELECT t FROM Turno t where t.usuarioComercio = ?1 AND t.fecha = ?2")
	public List<Turno> traerTurnosPorUsuarioYFecha(String usuarioComercio, Date fecha);

	@Query("SELECT t from Turno t where t.usuarioComercio=?1 AND EXTRACT(MONTH FROM fecha)= ?2 AND EXTRACT(YEAR FROM fecha)= ?3 ")
	public List<Turno> traerTurnosPorUsuarioMesAño(String usuarioComercio,Integer mes, Integer año);

}
